board=$1
sketch=$2
dev="/dev/ttyACM0"

if [ "$#" -ne 2 ]
then
    echo "Usage: $0 <board> <sketch>"
    exit 1
fi

if [ "$board" == "nano" ]
then
    fqbn="arduino:megaavr:nona4809"
elif [ "$board" == "uno" ]
then
    fqbn="arduino:avr:uno"
else
    echo "Board $board not recognized"
    exit 1
fi

arduino-cli compile --fqbn $fqbn $sketch
arduino-cli upload -p $dev --fqbn $fqbn $sketch
