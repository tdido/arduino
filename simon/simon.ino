#include "notes.h"

const int buzzer_pin = 8;

//arrays to store frequency, led pin, and button pin for each button
//values with the same index correspond to the same button
const int btn_notes[] = {NOTE_E5, NOTE_CS5, NOTE_A5, NOTE_E4};
const int led_pins[] = {12,11,9,10};
const int btn_pins[] = {5,4,2,3};

//arrays for melodies, played in order
const int melody_start[] = {NOTE_G4,NOTE_G4,NOTE_G4,NOTE_C5};
const int melody_start_d[] = {300,150,150,750};
const int melody_lose[] = {NOTE_AS4,NOTE_A4,NOTE_GS4,NOTE_G4};
const int melody_lose_d[] = {500,500,500,1500};
const int melody_win[] = {NOTE_C4,NOTE_F4,NOTE_G4,NOTE_A4,NOTE_G4,NOTE_E4,NOTE_C4,NOTE_F4,NOTE_G4,NOTE_A4,NOTE_G4,NOTE_C4,NOTE_F4,NOTE_G4,NOTE_A4,NOTE_G4,NOTE_E4,NOTE_E4,NOTE_F4,NOTE_E4,NOTE_C4,NOTE_C4};
const int melody_win_d[] = {100,100,100,100,300,600,100,100,100,100,800,100,100,100,100,300,600,100,100,100,100,800};

//number of random states (length of the game - 1)
int nstates = 4; //default states, 0 counts so set to n-1
int *states;

//hold down each button to set the duration
const int button_nstates[] = {9,14,19,24};


int state_max_idx = 0;
int state_idx = 0;
int turn = 0; // 0=simon, 1=human

void play_note(int note, int duration){
    tone(buzzer_pin, note, duration);
    delay(duration);
    noTone(buzzer_pin);
    delay(20);
}

void start_game(){
    turn = 0;
    state_idx = 0;
    state_max_idx = 0;

    for(int i=0;i<=3;i++){
        play_note(melody_start[i],melody_start_d[i]);
        delay(20);
    }
    delay(500);

    //fill the random states array with indexes
    for (int i = 0; i <= nstates; i++) {
        states[i] = random(4);
    }
}

void setup(){
    //Serial.begin(9600);
    //start_game the random seed with noise form an analog input
    randomSeed(analogRead(0));

    pinMode(buzzer_pin, OUTPUT);

    //intialise all button and led pins
    for(int i=0;i<=3;i++){
        pinMode(btn_pins[i], INPUT_PULLUP);
        pinMode(led_pins[i], OUTPUT);

        if(digitalRead(btn_pins[i]) == HIGH){
            nstates = button_nstates[i]; 
        }
    }

    states = (int *) malloc((nstates+1)*sizeof(int));

    start_game();
}

//function to beep and turn on a led
void toggle(int idx){
    tone(buzzer_pin, btn_notes[idx],500);
    digitalWrite(led_pins[idx], HIGH);
    delay(500);
    digitalWrite(led_pins[idx], LOW);
    noTone(buzzer_pin);
    delay(100);
}

void loop(){
    if(turn == 0){ //simon's turn
        for (int i=0; i<=state_max_idx; i++) {
            toggle(states[i]); 
        }
        turn = 1;
    }else{ //human's turn
        //loop all the indexes
        for (int i=0; i<=3; i++) {
            //if the button with the current index is active
            //then beep at its frequency and turn on its led
            if(digitalRead(btn_pins[i]) == HIGH){
                toggle(i);
                if(i == states[state_idx]){ //the button pressed matches the current state
                    if(state_idx == state_max_idx){//the current state is the max state
                        if(state_idx == nstates){ //the current state is the end of the game
                            //win

                            int j=0;
                            for(int i=0;i<=21;i++){
                                digitalWrite(led_pins[j], HIGH);
                                play_note(melody_win[i],melody_win_d[i]);
                                digitalWrite(led_pins[j], LOW);
                                if(j++ > 3)
                                    j=0;
                            }

                            start_game();
                        }else{//max state, but not the end of the game
                            turn=0;
                            state_max_idx++;
                            state_idx=0;
                            delay(1000);
                        }
                    }else{//not reached the max state yet
                        state_idx++;
                    }
                }else{
                    //lose
                    for(int i=0;i<=3;i++){
                        digitalWrite(led_pins[i], HIGH);
                        play_note(melody_lose[i],melody_lose_d[i]);
                        digitalWrite(led_pins[i], LOW);
                    }
                    delay(500);
                    start_game();
                }
            }
        }
    }
}
